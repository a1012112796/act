package jobparser

import (
	"embed"
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"

	"github.com/stretchr/testify/require"

	"gopkg.in/yaml.v3"
)

//go:embed testdata
var f embed.FS

func TestParse(t *testing.T) {
	tests := []struct {
		name    string
		options []ParseOption
		wantErr bool
	}{
		{
			name:    "multiple_jobs",
			options: nil,
			wantErr: false,
		},
		{
			name:    "multiple_matrix",
			options: nil,
			wantErr: false,
		},
		{
			name:    "has_needs",
			options: nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			content, err := f.ReadFile(filepath.Join("testdata", tt.name+".in.yaml"))
			require.NoError(t, err)
			want, err := f.ReadFile(filepath.Join("testdata", tt.name+".out.yaml"))
			require.NoError(t, err)
			got, err := Parse(content, tt.options...)
			if tt.wantErr {
				require.Error(t, err)
			}
			require.NoError(t, err)

			builder := &strings.Builder{}
			for _, v := range got {
				if builder.Len() > 0 {
					builder.WriteString("---\n")
				}
				encoder := yaml.NewEncoder(builder)
				encoder.SetIndent(2)
				_ = encoder.Encode(v)
			}
			assert.Equal(t, string(want), builder.String())
		})
	}
}
